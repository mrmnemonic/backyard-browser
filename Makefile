CC=gcc
CFLAGS=`pkg-config --cflags --libs gtk+-2.0 webkit-1.0`

backyard: main.c
	${CC} -DDEBUG -o backyward main.c ${CFLAGS}

release: main.c
	${CC} -o backyward main.c ${CFLAGS}

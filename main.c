/*
 * backyard.c
 * Walled-garden mini browser
 * By Martin Coleman (martin.coleman@monash.edu)
 * for e-Exam (transformingexams.com) by Mathew Hillier
 * Licensed under the BSD "Simplified" license.
 * 
 * Compile with:
 * gcc -o backyward main.c `pkg-config --cflags --libs gtk+-2.0 webkit-1.0`
 * 
 * First version- 2016-10-20
 * - Basic web browser controls
 * - Whitelist function
 * 
 * 2016-10-28
 * - Added fullscreen functionality
 */
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <webkit/webkit.h>

#define MAX_SITES 16
//#define PRG_VERSION "1.1"
//#define DEBUG 1

GtkWidget *main_window;
GtkWidget *web_view;
GtkWidget *vbox;
//GtkWidget *entry;

const char* uri;

typedef struct {
	char url[2000];
} sites;
sites whitelist[MAX_SITES];
int site_number=0;

// You must free the result if result is non-NULL.
char *str_replace(char *orig, char *rep, char *with) {
    char *result; // the return string
    char *ins;    // the next insert point
    char *tmp;    // varies
    int len_rep;  // length of rep
    int len_with; // length of with
    int len_front; // distance between rep and end of last rep
    int count;    // number of replacements

    if (!orig)
        return NULL;
    if (!rep)
        rep = "";
    len_rep = strlen(rep);
    if (!with)
        with = "";
    len_with = strlen(with);

    ins = orig;
    for (count = 0; tmp = strstr(ins, rep); ++count) {
        ins = tmp + len_rep;
    }

    // first time through the loop, all the variable are set correctly
    // from here on,
    //    tmp points to the end of the result string
    //    ins points to the next occurrence of rep in orig
    //    orig points to the remainder of orig after "end of rep"
    tmp = result = malloc(strlen(orig) + (len_with - len_rep) * count + 1);

    if (!result)
        return NULL;

    while (count--) {
        ins = strstr(orig, rep);
        len_front = ins - orig;
        tmp = strncpy(tmp, orig, len_front) + len_front;
        tmp = strcpy(tmp, with) + len_with;
        orig += len_front + len_rep; // move to next "end of rep"
    }
    strcpy(tmp, orig);
    return result;
}

void backyard_whitelist()
{
	FILE *fp;
	char line_buffer[2000];
	int i;
	
	/* read whitelist into array */
	fp=fopen("whitelist.lst", "r");
	if (fp==NULL)
	{
		printf("Unable to find whitelist.\n");
		exit(1);
	}
	while (fgets(line_buffer, 2000, fp) != NULL)
	{
		/*
		strncpy(URL_whitelist[i]->url, line_buffer, strlen(line_buffer));
		printf("DEBUG: %s\n", URL_whitelist[i]->url);
		*/
		line_buffer[strlen(line_buffer)-1]='\0';

		#ifdef DEBUG
		printf("URL: %s\n", line_buffer);
		#endif

		strcpy(whitelist[site_number].url, line_buffer);
		if (site_number==MAX_SITES) break;
		site_number++;
		line_buffer[0]='\0';
	}

	#ifdef DEBUG
	printf("DEBUG: Fetched %d sites.\n", site_number);
	for(i=0; i<site_number;i++)
	{
		printf("Site %d: %s\n", i, whitelist[i].url);
	}
	#endif

	fclose(fp);
	return;
}

int check_whitelist(const char *check_url)
{
	int i;
	char *cleaned_url;

	cleaned_url=strdup(check_url);
	cleaned_url=str_replace(cleaned_url, "http://", "");
	cleaned_url=str_replace(cleaned_url, "https://", "");

	#ifdef DEBUG
	printf("DEBUG: Checking site [%s]\n", cleaned_url);
	#endif

	for(i=0; i<site_number; i++)
	{
		if(!strstr(whitelist[i].url, cleaned_url))
		{
			#ifdef DEBUG
			printf("DEBUG: Site is good.\n");
			#endif

			return 1; /* found it! */
		}
	}

	#ifdef DEBUG
	printf("DEBUG: Site is not good.\n");
	#endif

	return 0;
}

/*
void go(GtkWidget* widget, gpointer* wid)
{
	GtkWidget* entry = GTK_WIDGET(wid);
	uri = gtk_entry_get_text(GTK_ENTRY(entry));
	if(check_whitelist(uri))
	{
		gtk_entry_progress_pulse(GTK_ENTRY(entry));
		webkit_web_view_load_uri(WEBKIT_WEB_VIEW(web_view),uri);
	}
}
*/

void back(GtkWidget* widget, gpointer* wid)
{
	//GtkWidget* web = GTK_WIDGET(wid);
	webkit_web_view_go_back(WEBKIT_WEB_VIEW(web_view));
}

void forward(GtkWidget* widget, gpointer* wid)
{
	//GtkWidget* web = GTK_WIDGET(wid);
	webkit_web_view_go_forward(WEBKIT_WEB_VIEW(web_view));
}

void stop(GtkWidget* widget, gpointer* wid)
{
	//GtkWidget* web = GTK_WIDGET(wid);
	webkit_web_view_stop_loading(WEBKIT_WEB_VIEW(web_view));
}

void refresh(GtkWidget* widget, gpointer* wid)
{
	//webkit_web_view_refresh(WEBKIT_WEB_VIEW(web_view));
}

void activate(GtkWidget* widget, gpointer* wid)
{
	//GtkWidget* web = GTK_WIDGET(wid);
	uri = gtk_entry_get_text(GTK_ENTRY(widget));
	if(check_whitelist(uri))
	{
		gtk_entry_progress_pulse(GTK_ENTRY(widget));
		webkit_web_view_load_uri(WEBKIT_WEB_VIEW(web_view),uri);
	}
}

void progress(WebKitWebView *web, gint progress,gpointer* data)
{
	GtkWidget* wid = GTK_WIDGET(data);
	gtk_entry_set_progress_fraction(GTK_ENTRY(wid),progress);
}
 
void finished(WebKitWebView  *web_view, WebKitWebFrame *frame, gpointer*  data)
{
	GtkWidget* wid = GTK_WIDGET(data);
	gtk_entry_set_progress_fraction(GTK_ENTRY(wid),0.0);
}

/* Create window */
static GtkWidget* create_window()
{
    GtkWidget* window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(window), "e-Exam Backyard v1.1");
	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW (window), 800, 600);
    g_signal_connect(G_OBJECT (window), "destroy", G_CALLBACK(gtk_main_quit), NULL);

    return window;
}

static GtkWidget* create_browser()
{
	WebKitWebSettings *settings;

    GtkWidget* scrolled_window = gtk_scrolled_window_new (NULL, NULL);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

    web_view = WEBKIT_WEB_VIEW(webkit_web_view_new());
    //web_view = webkit_web_view_new();
    gtk_container_add (GTK_CONTAINER (scrolled_window), GTK_WIDGET (web_view));

	g_signal_connect(WEBKIT_WEB_VIEW(web_view), "load-progress-changed", G_CALLBACK(progress), web_view);
	g_signal_connect(WEBKIT_WEB_VIEW(web_view), "load-finished", G_CALLBACK(finished), web_view);
	g_signal_connect(WEBKIT_WEB_VIEW(web_view), "clicked", G_CALLBACK(activate), NULL);
	//g_signal_connect(WEBKIT_WEB_VIEW(web_view), "load-committed", G_CALLBACK(load_commit), web_view);
	//g_signal_connect(WEBKIT_WEB_VIEW(web_view), "hovering-over-link", G_CALLBACK(link_hover), web_view);

	settings = webkit_web_view_get_settings(WEBKIT_WEB_VIEW(web_view));
	
	/* Set SEB compatibility Hash header */
	g_object_set(G_OBJECT(settings), "user-agent", "Mozilla/5.0 (X11; U; Unix; en-US) " "AppleWebKit/537.15 (KHTML, like Gecko) " "BACKYARD/1.1 SEB", NULL);
	g_object_set(G_OBJECT(settings), "X-SafeExamBrowser-RequestHash", "1234567890123456789012345678901234567890123456789012345678901234", NULL);

	return scrolled_window;
}

static GtkWidget*create_toolbar ()
{
    GtkWidget* toolbar = gtk_toolbar_new ();

    gtk_toolbar_set_orientation (GTK_TOOLBAR (toolbar), GTK_ORIENTATION_HORIZONTAL);
    gtk_toolbar_set_style (GTK_TOOLBAR (toolbar), GTK_TOOLBAR_BOTH_HORIZ);

    GtkToolItem* item;

    /* the back button */
    item = gtk_tool_button_new_from_stock (GTK_STOCK_GO_BACK);
    g_signal_connect (G_OBJECT (item), "clicked", G_CALLBACK (back), NULL);
    gtk_toolbar_insert (GTK_TOOLBAR (toolbar), item, -1);

    /* The forward button */
    item = gtk_tool_button_new_from_stock (GTK_STOCK_GO_FORWARD);
    g_signal_connect (G_OBJECT (item), "clicked", G_CALLBACK (forward), NULL);
    gtk_toolbar_insert (GTK_TOOLBAR (toolbar), item, -1);

    /* The refresh button */
    /*
    item = gtk_tool_button_new_from_stock (GTK_STOCK_GO_REFRESH);
    g_signal_connect (G_OBJECT (item), "clicked", G_CALLBACK (refresh), NULL);
    gtk_toolbar_insert (GTK_TOOLBAR (toolbar), item, -1);
    */

    return toolbar;
}

int main(int argc, char** argv)
{
	int fullscreen=0;

	gtk_init(&argc,&argv);
	backyard_whitelist();

	/* Do we want fullscreen or not? */
	if(getenv("BACKYARD_FULLSCREEN")) fullscreen=1;

	GtkWidget* vbox = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), create_toolbar (), FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), create_browser(), TRUE, TRUE, 0);

	main_window = create_window();
	gtk_container_add (GTK_CONTAINER (main_window), vbox);
	if (fullscreen) gtk_window_fullscreen(GTK_WINDOW(main_window));
	
	gtk_widget_grab_focus(GTK_WIDGET(web_view));
	if (argc>1) webkit_web_view_load_uri ((WebKitWebView *) web_view, argv[1]);
	gtk_widget_show_all(main_window);
	gtk_main();
	return 0;
}
